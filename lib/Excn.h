#ifndef _EXCN_H
#define _EXCN_H
#include <string>
#include <exception>
#include <iostream>
#include <sstream>
#include <boost/exception/to_string.hpp>

class Err : public std::exception {
private:
  std::string m;
public:
  Err(int line, const char *file, std::string msg) {
    m = "E: L("+boost::to_string<int>(line)+") F: "+std::string(file)+" "+msg;
  }

  virtual ~Err(void) throw() { }

  virtual const char *what() const throw() { return m.c_str(); }
};

#define ASSERT(cond, msg) if(!(cond)) throw Err(__LINE__, __FILE__, msg);
#define NIY(msg) throw Err(__LINE__, __FILE__, std::string("NIY: ")+std::string(msg));

#endif
