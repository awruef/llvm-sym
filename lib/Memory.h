#ifndef _MEMORY_H
#define _MEMORY_H

#include <boost/icl/interval.hpp>
#include <boost/icl/interval_map.hpp>
#include <boost/cstdint.hpp>

class MemObject { 
private:
public:
  MemObject() {
    return;
  }

  ~MemObject() {
  }

  MemObject operator +=(const MemObject &rhs) {
    return *this;
  }

  MemObject operator=(const MemObject &rhs) {
    return *this;
  }

  bool operator==(const MemObject &rhs) const {
    return false;
  }
};

typedef boost::icl::interval_map<boost::uint64_t, MemObject>  AddrMap;

#endif
