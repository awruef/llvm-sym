#ifndef _MACHINE_H
#define _MACHINE_H

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

class Machine : public boost::enable_shared_from_this<Machine> {
private:

public:
  virtual bool evaluateAtFunction(llvm::Function *) = 0;

  virtual bool runTick(void) = 0;
};

#endif
