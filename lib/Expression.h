#ifndef _EXPRESSION_H
#define _EXPRESSION_H

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "llvm/IR/Instruction.h"

//virtual base class of Expressions
class _Expression : public boost::enable_shared_from_this<_Expression> {
private:

public:

};

typedef boost::shared_ptr<_Expression> Expression;

Expression exprBuilder(llvm::Instruction *);

#endif
