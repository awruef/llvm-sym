#ifndef _SYMBOLIC_MACHINE_H
#define _SYMBOLIC_MACHINE_H

#include "llvm/IR/Module.h"

#include "Machine.h"
#include "State.h"
#include "Expression.h"
#include "Memory.h"

class SymbolicMachine : public Machine {
private:
  AddrMap             memory;
  llvm::Module        *M;
  std::vector<State>  states;

public:

};

#endif
