#ifndef _STATE_H
#define _STATE_H

#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "Expression.h"

class _State : public boost::enable_shared_from_this<_State> {
private:
  //constraints present in this state
  std::vector<Expression> constraints;

public:

};

typedef boost::shared_ptr<_State> State;

#endif
